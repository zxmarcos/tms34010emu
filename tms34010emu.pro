TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

QT += widgets core

DEPENDPATH += .

QMAKE_CXXFLAGS += -Wno-unused-parameter

SOURCES += main.cpp \
    tms34010.cpp \
    memory.cpp \
    boot.cpp \
    tms34010_dasm.cpp \
    ui/dasmviewex.cpp \
    ui/hexspinbox.cpp \
    ui/mainwindow.cpp \
    ui/registersview.cpp \
    serialpic.cpp



HEADERS += \
    tms34010.h \
    memory.h \
    tms34010_defs.h \
    tms34010_arithm.h \
    tms34010_memacc.h \
    tms34010_ctrl.h \
    tms34010_rw.h \
    tms34010_helper.h \
    ui/dasmviewex.h \
    ui/hexspinbox.h \
    ui/mainwindow.h \
    ui/registersview.h \
    tms34010_bitops.h \
    tms34010_branch.h \
    tms34010_shift.h \
    serialpic.h

RESOURCES += \
    ui/rscr.qrc

