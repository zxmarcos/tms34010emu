/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_CTRL
#define TMS34010_CTRL

#include "tms34010.h"
#include "tms34010_memacc.h"
#include "tms34010_defs.h"
#include "tms34010_helper.h"

namespace tms
{

inline void tms34010::SETF(uint16_t opcode)
{
    if (opcode & (1 << 9)) {
        ST &= ~(ST_FS1_MASK | ST_FE1);
        ST |= ((opcode & 0x1F) << ST_FS1_SHIFT) & ST_FS1_MASK;
        if (opcode & (1 << 5))
            ST |= ST_FE1;
    } else {
        ST &= ~(ST_FS0_MASK | ST_FE0);
        ST |= (opcode & 0x1F) & ST_FS0_MASK;
        if (opcode & (1 << 5))
            ST |= ST_FE0;
    }
}

inline void tms34010::DINT(uint16_t)
{
    ST &= ~ST_IE;
}

inline void tms34010::SETC(uint16_t opcode)
{
    ST |= ST_C;
}

inline void tms34010::CLRC(uint16_t opcode)
{
    ST &= ~ST_C;
}

inline void tms34010::MMTM_rp_list(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    const int base = (opcode & 0x10) ? R_B : 0;
    uint16_t list = read16_aligned(PC);

    ST &= ~ST_N;
    if (!rd)
        ST |= ST_N;
    else {
        if ((0 - rd) & SIGN_BIT32 && rd != 0x80000000)
            ST |= ST_N;
    }


    for (int i = 0; i < 15; i++) {
        if (list & 0x8000) {
            rd -= 32;
            write32_aligned(rd, m_state.r[base + i]);
        }
        list <<= 1;
    }
    // PUSH ST last
    if (list & 0x8000) {
        rd -= 32;
        write32_aligned(rd, SP_v);
    }

    PC += 16;
}

inline void tms34010::MMFM_rp_list(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    const int base = (opcode & 0x10) ? R_B : 0;
    uint16_t list = read16_aligned(PC);

    // POP SP first
    if (list & 0x8000) {
         SP_v = read32_aligned(rd);
    }
    list <<= 1;

    for (int i = 14; i >= 0; i--) {
        if (list & 0x8000) {
            m_state.r[base + i].value = read32_aligned(rd);
            rd += 32;
        }
        list <<= 1;
    }

    PC += 16;
}

inline void tms34010::RETS_n(uint16_t opcode)
{
    PC = read32_aligned(SP_v);
    SP_v += 32 + NF * 16;
}

inline void tms34010::POPST(uint16_t opcode)
{
    ST = read32_aligned(SP_v);
    SP_v += 32;
}

inline void tms34010::PUSHST(uint16_t opcode)
{
    SP_v -= 32;
    write32_aligned(SP_v, ST);
}

inline void tms34010::EXGPC_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t tmp = PC;
    PC = rd;
    rd = tmp;
}

}

#endif // TMS34010_CTRL

