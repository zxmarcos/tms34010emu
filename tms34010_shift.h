#ifndef TMS34010_SHIFT_H
#define TMS34010_SHIFT_H

#include "tms34010.h"
#include "tms34010_defs.h"
#include "tms34010_helper.h"


namespace tms
{

inline void tms34010::SLL_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t k = K & 0x1F;
    ST &= ~ST_C;
    if (k) {
        if (rd & 0x80000000)
            ST |= ST_C;
        rd <<= k;
    }
    Z_UPDATE(rd);
}

inline void tms34010::SRL_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t k = K2C;
    ST &= ~ST_C;
    if (k) {
        if (rd & 1)
            ST |= ST_C;
        rd >>= k;
    }
    Z_UPDATE(rd);
}

}

#endif // TMS34010_SHIFT_H

