/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_HELPER_H
#define TMS34010_HELPER_H

#include "tms34010.h"
#include "tms34010_defs.h"

namespace tms
{

inline void tms34010::ZN_UPDATE(uint32_t value)
{
    ST &= ~(ST_N | ST_Z);
    if (value & SIGN_BIT32)
        ST |= ST_N;
    if (!value)
        ST |= ST_Z;
}

inline void tms34010::Z_UPDATE(uint32_t value)
{
    if (value)
        ST &= ~ST_Z;
    else
        ST |= ST_Z;
}


//    /* 0000 */ "",
//    /* 0001 */ "lo",
//    /* 0010 */ "ls",
//    /* 0011 */ "hi",
//    /* 0100 */ "lt",
//    /* 0101 */ "ge",
//    /* 0110 */ "le",
//    /* 0111 */ "gt",
//    /* 1000 */ "c",
//    /* 1001 */ "nc",
//    /* 1010 */ "eq",
//    /* 1011 */ "ne",
//    /* 1100 */ "v",
//    /* 1101 */ "nv",
//    /* 1110 */ "n",
//    /* 1111 */ "nn"
template<uint8_t type>
inline bool tms34010::COND_EVAL()
{
    switch (type) {
    case 0x00 /* UC */: return true;
    case 0x01 /* LO */: return CC;
    case 0x02 /* LS */: return CC || ZZ;
    case 0x03 /* HI */: return !CC && !ZZ;
    case 0x04 /* LT */: return (NN && !VV) || (!NN && VV);
    case 0x05 /* GE */: return (NN && VV) || (!NN && !VV);
    case 0x06 /* LE */: return (NN && VV || (!NN && VV) || ZZ);
    case 0x07 /* GT */: return (NN && VV && !ZZ) || (!NN && !VV & !ZZ);
    case 0x08 /* C  */: return CC;
    case 0x09 /* NC */: return !CC;
    case 0x0A /* EQ */: return ZZ;
    case 0x0B /* NE */: return !ZZ;
    case 0x0C /* V  */: return VV;
    case 0x0D /* NV */: return !VV;
    case 0x0E /* N  */: return NN;
    case 0x0F /* NN */: return !NN;
    default:
        return false;
    }
}

}

#endif // TMS34010_HELPER_H

