/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#include <iostream>
#include <cstdio>
#include "tms34010.h"
#include "tms34010_defs.h"
#include "tms34010_memacc.h"
#include "tms34010_helper.h"
#include "tms34010_arithm.h"
#include "tms34010_bitops.h"
#include "tms34010_branch.h"
#include "tms34010_ctrl.h"
#include "tms34010_shift.h"
#include "tms34010_rw.h"
#include "memory.h"


namespace tms
{

tms34010 *g_tms = nullptr;

tms34010::tms34010()
{
    m_log.open("emu-trace.txt", ios_base::out | ios_base::trunc);
    g_tms = this;
}

void tms34010::reset()
{
    ST = 0x00000010;
    PC = read32_aligned(VECT_RESET);
    for (int i = 0; i < 14; i++) {
        A(i) = 0;
        B(i) = 0;
    }
}


void tms34010::bp_insert(addr_t address)
{
    m_breakpoints.insert(address);
}

void tms34010::bp_remove(addr_t address)
{
    m_breakpoints.erase(m_breakpoints.find(address));
}

inline bool tms34010::check_breakpoint()
{
    return (m_breakpoints.find(PC) != m_breakpoints.end());
}

int tms34010::run(int cycles, bool skip_bps)
{
    int icount = 0;

    while (icount < cycles) {

        if (!skip_bps && check_breakpoint()) {
            m_reason = STOP_BREAKPOINT_FOUND;
            return cycles - icount;
        }

        uint16_t opcode = read16_aligned(PC);
        //m_log << dasm(opcode, PC) << endl;
        LPC = PC;
        PC += 16;

        switch (opcode >> 12) {
        case 0x00: /* W: 1 or 0 register format  */
            switch ((opcode >> 5) & 0x7F) {

            // DINT
            case 0x1B: DINT(opcode); break;
            case 0x0E: POPST(opcode); break;
            case 0x0F: PUSHST(opcode); break;
            case 0x0B: JUMP_rs(opcode); break;
            case 0x09: EXGPC_rd(opcode); break;
            case 0x1F: NOT_rd(opcode); break;

            // SETF fs, fe, f
            case 0x2A:
            case 0x2B:
            case 0x3A:
            case 0x3B: SETF(opcode); break;

            // MOVI iw, rd
            case 0x4E: MOVI_w(opcode); break;
            // MOVI il, rd
            case 0x4F: MOVI_l(opcode); break;

            // MOVE rs, @DAddr, 1
            case 0x3C: MOVE_r_2_abs_1(opcode); break;
            // MOVE rs, @DAddr, 0
            case 0x2C: MOVE_r_2_abs_0(opcode); break;

            // MOVE @SAddr, rd, 1
            case 0x3D: MOVE_abs_2_r_1(opcode); break;
            // MOVE @SAddr, rd, 0
            case 0x2D: MOVE_abs_2_r_0(opcode); break;

            case 0x2F: MOVB_rs_da(opcode); break;

            case 0x4B: RETS_n(opcode); break;

            case 0x4C: MMTM_rp_list(opcode); break;
            case 0x4D: MMFM_rp_list(opcode); break;

            case 0x58: ADDI_iw_rd(opcode); break;
            case 0x59: ADDI_il_rd(opcode); break;
            case 0x5C: ANDI_il_rd(opcode); break;
            case 0x5F: SUBI_iw_rd(opcode); break;
            case 0x68: SUBI_il_rd(opcode); break;

            case 0x5A: CMPI_iw_rd(opcode); break;
            case 0x5B: CMPI_il_rd(opcode); break;

            case 0x6C: DSJ_rd_disp(opcode); break;

            case 0x69: {
                switch (opcode & 0xFF) {
                case 0x3F: CALLR(opcode); break;
                }
                break;
            }

            case 0x6A: {
                switch (opcode & 0xFF) {
                case 0x5F: CALLA(opcode); break;
                }
                break;
            }

            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 0]\n", LPC, opcode, (opcode >> 5) & 0x7F);
                break;

            }
            break;

        case 0x01: /* X: Short constant format   */
            switch ((opcode >> 10) & 3) {
            case 0x03: BTST_k_rd(opcode); break;
            case 0x02: MOVK_k_rd(opcode); break;
            case 0x01: SUBK_k_rd(opcode); break;
            case 0x00: ADDK_k_rd(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 1]\n", LPC, opcode, (opcode >> 10) & 3);
                break;
            }
            break;

        case 0x02:
            switch ((opcode >> 10) & 3) {
            case 0x01: SLL_k_rd(opcode); break;
            case 0x03: SRL_k_rd(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 2]\n", LPC, opcode, (opcode >> 10) & 3);
                break;
            }
            break;

        case 0x03:
            if (opcode & (1 << 11))
                DSJS(opcode);
            else {
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 3]\n", LPC, opcode, (opcode >> 5) & 0x7F);
            }
            break;

        case 0x04:
            switch ((opcode >> 9) & 7) {
            case 0x0: ADD_rs_rd(opcode); break;
            case 0x2: SUB_rs_rd(opcode); break;
            case 0x4: CMP_rs_rd(opcode); break;
            case 0x6: MOVE_rs_rd(opcode); break;
            case 0x7: R_BIT ? MOVE_rs_rd_ba(opcode) : MOVE_rs_rd_ab(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 4]\n", LPC, opcode, (opcode >> 9) & 7);
                break;
            }
            break;

        case 0x05:
            switch ((opcode >> 9) & 7) {
            case 0x00: AND_rs_rd(opcode); break;
            case 0x01: ANDN_rs_rd(opcode); break;
            case 0x02: OR_rs_rd(opcode); break;
            case 0x03: XOR_rs_rd(opcode); break;
            case 0x05: DIVU_rs_rd(opcode); break;
            case 0x07: MPYU_rs_rd(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 5]\n", LPC, opcode, (opcode >> 9) & 7);
                break;
            }
            break;

        case 0x06:
            switch ((opcode >> 9) & 7) {
            case 0x05: LMO_rs_rd(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: 6]\n", LPC, opcode, (opcode >> 9) & 7);
                break;
            }
            break;

        case 0x08:
            switch ((opcode >> 9) & 7) {
            case 0x07: MOVB_i_rs_rd(opcode); break;
            case 0x06: MOVB_rs_i_rd(opcode); break;
            case 0x02: MOVE_i_rs_rd_0(opcode); break;
            case 0x03: MOVE_i_rs_rd_1(opcode); break;
            case 0x00: MOVE_rs_i_rd_0(opcode); break;
            case 0x01: MOVE_rs_i_rd_1(opcode); break;
            default:
                tms_log("$0x%08X [%4X] unimplemented [TYPE: 8]\n", LPC, opcode);
                break;
            }
            break;

        case 0x09:
            switch ((opcode >> 9) & 7) {
            case 0x00: MOVE_rs_i_rd_pi_0(opcode); break;
            case 0x01: MOVE_rs_i_rd_pi_1(opcode); break;
            case 0x02: MOVE_i_rs_pi_rd_0(opcode); break;
            case 0x03: MOVE_i_rs_pi_rd_1(opcode); break;
            case 0x04: MOVE_i_rs_pi_i_rd_pi_0(opcode); break;
            case 0x05: MOVE_i_rs_pi_i_rd_pi_1(opcode); break;
            default:
                tms_log("$0x%08X [%4X] unimplemented [TYPE: 9]\n", LPC, opcode);
                break;
            }
            break;

        case 0x0C: /* Y: Conditional jump format */
            if (opcode & 0xFF) {
                if ((opcode & 0xFF) == 0x80) { // JA addr
                    JACC(opcode);
                    break;
                }
                // JR shor addr
                JRCC(opcode);
                break;
            } else {
                JRCC_rel(opcode);
                break;
            }

            tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: C]\n", LPC, opcode);
            break;

        case 0x0E:
            switch ((opcode >> 9) & 7) {
            case 0x6: MOVX_rs_rd(opcode); break;
            case 0x7: MOVY_rs_rd(opcode); break;
            default:
                tms_log("$0x%08X [%4X] [%2X] unimplemented [TYPE: E]\n", LPC, opcode);
                break;
            }

            break;

        default:   /* Z: 2 registers format      */
            tms_log("$0x%08X [%4X] [%2X] unimplemented [X]\n", LPC, opcode);
            break;
        }
        icount++;

    }
    m_reason = STOP_CYCLES_EXPIRED;
    return cycles - icount;
}



}
