/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_RW
#define TMS34010_RW

#include <QDebug>

#include "tms34010.h"
#include "tms34010_memacc.h"
#include "tms34010_defs.h"
#include "tms34010_helper.h"

namespace tms
{

inline void tms34010::MOVI_w(uint16_t opcode)
{
    int32_t data = read16_aligned(PC);

    if (RD == R_SP) {
        SP_v = data;
    } else {
        RD_v = data;
    }

    PC += 16;
    ZN_UPDATE(data);
}

inline void tms34010::MOVI_l(uint16_t opcode)
{
    int32_t data = read32_aligned(PC);

    if (RD == R_SP) {
        SP_v = data;
    } else {
        RD_v = data;
    }

    PC += 32;
    ZN_UPDATE(data);
}


// MOVE rd, abs, 0
inline void tms34010::MOVE_r_2_abs_0(uint16_t opcode)
{
    uint32_t dst = read32_aligned(PC);
    if (RD == R_SP)
        write_field_0(dst, SP_v);
    else
        write_field_0(dst, RD_v);
    PC += 32;
}

// MOVE rd, abs, 1
inline void tms34010::MOVE_r_2_abs_1(uint16_t opcode)
{
    uint32_t dst = read32_aligned(PC);
    if (RD == R_SP)
        write_field_1(dst, SP_v);
    else
        write_field_1(dst, RD_v);
    PC += 32;
}

// MOVE abs, rd, 0
inline void tms34010::MOVE_abs_2_r_0(uint16_t opcode)
{
    uint32_t dst = read32_aligned(PC);
    if (RD == R_SP)
        SP_v = read_field_0(dst);
    else
        RD_v = read_field_0(dst);
    PC += 32;
}

// MOVE abs, rd, 1
inline void tms34010::MOVE_abs_2_r_1(uint16_t opcode)
{
    uint32_t dst = read32_aligned(PC);
    if (RD == R_SP)
        SP_v = read_field_1(dst);
    else
        RD_v = read_field_1(dst);
    PC += 32;
}


inline void tms34010::MOVK_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    rd = K;
}

inline void tms34010::MOVE_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    ST &= ~ST_V;
    rd = rs;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVE_rs_rd_ab(uint16_t opcode)
{
    uint32_t &rd = GET_RD_B();
    uint32_t rs = GET_RS_A();
    ST &= ~ST_V;
    rd = rs;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVE_rs_rd_ba(uint16_t opcode)
{
    uint32_t &rd = GET_RD_A();
    uint32_t rs = GET_RS_B();
    ST &= ~ST_V;
    rd = rs;
    ZN_UPDATE(rd);
}


inline void tms34010::MOVB_i_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();

    rd = read8_aligned_sx(rs);
    ST &= ~ST_V;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVB_rs_da(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t addr = read32_aligned(PC);

    write8_aligned(addr, rd);
    PC += 32;
}

inline void tms34010::MOVB_rs_i_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();

    rd = read8_aligned_sx(rs);
    ST &= ~ST_V;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVE_rs_i_rd_pi_0(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();

    write_field_0(rd, rs);
    rd += FS0;
}

inline void tms34010::MOVE_rs_i_rd_pi_1(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();

    write_field_1(rd, rs);
    rd += FS1;
}

inline void tms34010::MOVE_i_rs_pi_i_rd_pi_0(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t &rs = GET_RS_v();
    uint32_t src = read_field_0(rs);

    write_field_0(rd, src);
    rd += FS0;
    rs += FS0;
}

inline void tms34010::MOVE_i_rs_pi_i_rd_pi_1(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t &rs = GET_RS_v();
    uint32_t src = read_field_1(rs);

    write_field_1(rd, src);
    rd += FS1;
    rs += FS1;
}

inline void tms34010::MOVE_i_rs_pi_rd_0(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t &rs = GET_RS_v();
    uint32_t value = read_field_0(rs);
    rd = value;
    ST &= ~ST_V;
    ZN_UPDATE(rd);
    rs += FS0;
}

inline void tms34010::MOVE_i_rs_pi_rd_1(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t &rs = GET_RS_v();
    uint32_t value = read_field_1(rs);
    rd = value;
    ST &= ~ST_V;
    ZN_UPDATE(rd);
    rs += FS1;
}

inline void tms34010::MOVE_i_rs_rd_0(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    uint32_t value = read_field_0(rs);
    rd = value;
    ST &= ~ST_V;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVE_i_rs_rd_1(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    uint32_t value = read_field_1(rs);
    rd = value;
    ST &= ~ST_V;
    ZN_UPDATE(rd);
}

inline void tms34010::MOVE_rs_i_rd_0(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    write_field_0(rd, rs);
}

inline void tms34010::MOVE_rs_i_rd_1(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    write_field_1(rd, rs);
}



inline void tms34010::MOVX_rs_rd(uint16_t opcode)
{
    cpureg_t &rd = GET_RD_r();
    cpureg_t rs = GET_RS_r();
    rd.x = rs.x;
}

inline void tms34010::MOVY_rs_rd(uint16_t opcode)
{
    cpureg_t &rd = GET_RD_r();
    cpureg_t rs = GET_RS_r();
    rd.y = rs.y;
}

}

#endif // TMS34010_RW

