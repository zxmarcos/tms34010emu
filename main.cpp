/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#include <iostream>
#include "tms34010.h"
#include "memory.h"

#include "ui/mainwindow.h"

using namespace std;
using namespace tms;

int main(int argc, char **argv)
{
    load_boot_rom();

    ui_main(argc, argv);
    return 0;
}

