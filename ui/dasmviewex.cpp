#include <QtWidgets>
#include "dasmviewex.h"
#include "../tms34010.h"
#include "../tms34010_memacc.h"
#include "../memory.h"

DasmViewEx::DasmViewEx(QWidget *parent) :
    QAbstractScrollArea(parent)
{
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    verticalScrollBar()->setMaximum(0x100000000ULL >> 4);

    m_font = QFont("Ubuntu Mono", 12);
    setFont(m_font);

    QFontMetrics metrics(m_font);

    m_lineHeight = metrics.height();
    m_descent = metrics.descent();

    m_viewFirstAddr = 0;
    m_currentAddr = 0;
    m_selectedAddr = 0;

    connect(verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(verticalScroll(int)));

    viewport()->setFixedWidth(metrics.width('X') * 60);
}

DasmViewEx::~DasmViewEx()
{

}

void DasmViewEx::paintEvent(QPaintEvent *event)
{
    QPainter p(viewport());

    p.fillRect(event->rect(), QColor(255, 255, 220));
    int linesPerView = viewport()->rect().height() / m_lineHeight + 2;

    tms::addr_t adr = m_viewFirstAddr;
    if (linesPerView >= m_displayList.size())
        m_displayList.resize(linesPerView * 2);

    for (int i = 0; i < linesPerView; i++) {
        QString instr;

        int bits = 0;
        uint16_t opcode = tms::read16_aligned(adr);
        instr = m_cpu->dasm(opcode, adr, &bits).c_str();
        m_displayList[i] = adr;

        if (adr >= 0xFFFFFFFF)
            break;
        if (adr == m_currentAddr)
            p.fillRect(0, i * m_lineHeight, viewport()->width(), m_lineHeight, QColor(120, 200, 255));

        if (adr == m_selectedAddr)
            p.fillRect(0, i * m_lineHeight, viewport()->width(), m_lineHeight, QColor(255, 200, 200, 200));

        p.drawText(0, (i + 1) * m_lineHeight - m_descent, instr);

        adr += bits;

    }
}

void DasmViewEx::mousePressEvent(QMouseEvent *event)
{
    int line = event->y() / m_lineHeight;
    m_selectedAddr = m_displayList[line];
    viewport()->update();
}

tms::tms34010 *DasmViewEx::cpu() const
{
    return m_cpu;
}

void DasmViewEx::setCpu(tms::tms34010 *cpu)
{
    m_cpu = cpu;
}

void DasmViewEx::gotoAddress(tms::addr_t addr)
{
    int linesPerView = viewport()->rect().height() / m_lineHeight + 2;
    m_currentAddr = addr;
    if (qFind(m_displayList, addr) == m_displayList.end()) {
        int displace = (linesPerView / 2 - 1) * 16;
        verticalScrollBar()->setValue((addr - displace) / 16);
    }
    viewport()->update();
}

tms::addr_t DasmViewEx::selectedAddress()
{
    return m_selectedAddr;
}

void DasmViewEx::verticalScroll(int value)
{
    m_viewFirstAddr = value * 16;
    update();
}

void DasmViewEx::gotoCurrentAddress()
{
    m_currentAddr = m_cpu->m_state.pc;
    gotoAddress(m_currentAddr);
}


