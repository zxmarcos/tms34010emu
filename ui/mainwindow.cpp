#include <QApplication>
#include <QtWidgets>
#include "registersview.h"
#include "dasmviewex.h"
//#include "tlbviewex.h"
#include "mainwindow.h"
//#include "vramview.h"
#include <cstdio>
#include "../memory.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_cpu.reset();
    m_regView = new RegistersView(this);

    m_scrollReg = new QScrollArea();
    m_scrollReg->setWidget(m_regView);
    m_dockRegView = new QDockWidget("Registers");
    m_dockRegView->setWidget(m_scrollReg);
    addDockWidget(Qt::LeftDockWidgetArea, m_dockRegView);

    m_dasmView = new DasmViewEx();
    m_dasmView->setCpu(&m_cpu);
    m_dasmView->gotoAddress(m_cpu.m_state.pc);

    setupRegisterViews();

    setCentralWidget(m_dasmView);

    m_toolbar = new QToolBar("Execuçao", this);
    m_step = new QAction("Step", this);
    m_step->setIcon(QIcon(":/next-icon.png"));
    m_runToCursor = new QAction("Run to Cursor", this);
    m_runToCursor->setIcon(QIcon(":/run-to-cursor.png"));
    m_reset = new QAction("Reset", this);
    m_reset->setIcon(QIcon(":/reset.png"));
    m_gotoPC = new QAction("Goto PC", this);
    m_run = new QAction("Run", this);
    m_toolbar->addAction(m_step);
    m_toolbar->addAction(m_runToCursor);
    m_toolbar->addAction(m_reset);
    m_toolbar->addAction(m_gotoPC);
    m_toolbar->addAction(m_run);

    QAction *saveVram = new QAction("Dump VRAM", this);
    connect(saveVram, SIGNAL(triggered()), this, SLOT(dumpVram()));
    m_toolbar->addAction(saveVram);

    addToolBar(Qt::TopToolBarArea, m_toolbar);

    connect(m_step, SIGNAL(triggered()), this, SLOT(step()));
    connect(m_runToCursor, SIGNAL(triggered()), this, SLOT(runToCursor()));
    connect(m_reset, SIGNAL(triggered()), this, SLOT(reset()));
    connect(m_gotoPC, SIGNAL(triggered()), m_dasmView, SLOT(gotoCurrentAddress()));
    connect(m_run, SIGNAL(triggered()), this, SLOT(toogleRun()));

    m_timer.setInterval(0);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(step()));

    resize(800, 400);

    m_isRunningToCursor = false;
    m_isRunning = false;

    reset();
//    m_cpu.bp_insert(0xFFB09720);
//    m_cpu.bp_insert(0xFFAE8690);
//    m_cpu.bp_insert(0xFFAE84F0);
//    m_cpu.bp_insert(0xFFB09710);
//    m_cpu.bp_insert(0xFFB09970);
//    m_cpu.bp_insert(0xFFB09AD0); //divu
    m_cpu.bp_insert(0xFFB0A760);

//  PIC
//    m_cpu.bp_insert(0xFFB09A10);
//    m_cpu.bp_insert(0xFFB09810);
}

MainWindow::~MainWindow()
{
}

void MainWindow::step()
{
    if (m_isRunning) {
        m_cpu.run(1000);
        if (m_cpu.reason() == tms::STOP_BREAKPOINT_FOUND)
            toogleRun();

    } else {
        auto pc = m_cpu.m_state.pc;

        if (m_isRunningToCursor) {
            m_cpu.run(1000);
            if (m_cpu.reason() == tms::STOP_BREAKPOINT_FOUND) {
                if (m_cpu.m_state.pc == m_stopPC)
                    runToCursor();
            }
        } else {

            // o step pode ignorar os breakpoints
            m_cpu.run(1, true);
            updateRegisterViews();
            if (m_cpu.m_state.pc != pc)
                m_dasmView->gotoAddress(m_cpu.m_state.pc);
        }
    }
    fflush(stdout);
}

void MainWindow::runToCursor()
{
    if (m_timer.isActive()) {
        m_timer.stop();
        m_runToCursor->setIcon(QIcon(":/run-to-cursor.png"));
        m_isRunningToCursor = false;
        m_reset->setEnabled(true);
        updateRegisterViews();
        m_dasmView->gotoAddress(m_cpu.m_state.pc);
        m_cpu.bp_remove(m_stopPC);
    } else {
        m_stopPC = m_dasmView->selectedAddress();
        m_isRunningToCursor = true;
        m_runToCursor->setIcon(QIcon(":/cancel.png"));
        m_reset->setEnabled(false);
        m_cpu.bp_insert(m_stopPC);
        m_timer.start();
    }
}

void MainWindow::reset()
{
    tms::init_devices();
    m_cpu.reset();
    m_dasmView->gotoAddress(m_cpu.m_state.pc);
    updateRegisterViews();
}

void MainWindow::toogleRun()
{
    if (m_isRunning) {
        m_timer.stop();
        m_step->setEnabled(true);
        m_reset->setEnabled(true);
        m_runToCursor->setEnabled(true);
        m_run->setText("Run");
        m_isRunning = false;
        m_dasmView->gotoAddress(m_cpu.m_state.pc);
        updateRegisterViews();
    } else {
        m_step->setEnabled(false);
        m_reset->setEnabled(false);
        m_runToCursor->setEnabled(false);
        m_run->setText("Stop");
        m_isRunning = true;
        m_timer.start();
    }
}

void MainWindow::dumpVram()
{
    tms::save_vram();
}

void MainWindow::setupRegisterViews()
{
    m_regView->append(RegistersView::RegisterDesc("pc", &m_cpu.m_state.pc));
    m_regView->append(RegistersView::RegisterDesc("sp", &m_cpu.m_state.sp.value));
    m_regView->append(RegistersView::RegisterDesc("st", &m_cpu.m_state.st));

    for (int i = 0; i < 15; i++) {
        m_regView->append(RegistersView::RegisterDesc(m_cpu.reg_names[i],
                                                      &m_cpu.m_state.r[i].value));
    }

    for (int i = 0; i < 15; i++) {
        m_regView->append(RegistersView::RegisterDesc(m_cpu.reg_names[tms::R_B + i],
                                                      &m_cpu.m_state.r[tms::R_B + i].value));
    }
}

void MainWindow::updateRegisterViews()
{
    m_regView->update();
}


int ui_main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    return app.exec();
}
