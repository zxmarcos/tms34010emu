#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScrollArea>
#include <QTimer>
#include "../tms34010.h"

class RegistersView;
class DasmViewEx;
class QDockWidget;
class QAction;
class QToolBar;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void step();
    void runToCursor();
    void reset();
    void toogleRun();
    void dumpVram();
private:
    void setupRegisterViews();
    void updateRegisterViews();
    tms::tms34010 m_cpu;

    QToolBar *m_toolbar;
    RegistersView *m_regView;
    DasmViewEx *m_dasmView;
    QScrollArea *m_scrollReg;
    QDockWidget *m_dockRegView;
    QAction *m_step;
    QAction *m_reset;
    QAction *m_runToCursor;
    QAction *m_gotoPC;
    QAction *m_run;
    bool m_isRunningToCursor;
    bool m_isRunning;
    tms::addr_t m_stopPC;
    QTimer m_timer;
};

int ui_main(int argc, char **argv);

#endif // MAINWINDOW_H
