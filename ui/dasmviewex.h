#ifndef DASMVIEWEX_H
#define DASMVIEWEX_H

#include <QFont>
#include <QAbstractScrollArea>
#include <QVector>
#include "../tms34010.h"

class DasmViewEx : public QAbstractScrollArea
{
    Q_OBJECT

public:
    DasmViewEx(QWidget *parent=0);
    ~DasmViewEx();
    tms::tms34010 *cpu() const;
    void setCpu(tms::tms34010 *cpu);
    void gotoAddress(tms::addr_t addr);
    tms::addr_t selectedAddress();
public slots:
    void verticalScroll(int units);
    void gotoCurrentAddress();
protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
private:
    QVector<tms::addr_t> m_displayList;
    tms::addr_t m_viewFirstAddr;
    tms::addr_t m_currentAddr;
    tms::addr_t m_selectedAddr;
    tms::tms34010 *m_cpu;
    QFont m_font;
    int m_lineHeight;
    int m_descent;
};

#endif // DASMVIEWEX_H
