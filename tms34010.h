/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_H
#define TMS34010_H

#include <cstdint>
#include <string>
#include <fstream>
#include <unordered_set>

namespace tms
{

#define U64(x) x##ULL

#define tms_log(...) printf("tms: " __VA_ARGS__)

#define BIT2BYTE(bitaddr)   ((bitaddr) >> 3)

typedef uint32_t addr_t;
typedef uint32_t bitaddr_t;

using namespace std;

void load_boot_rom();

// sign extend word
#define SXW(x)  ((int16_t) (x))

enum {
    R_SP = 15,
    R_B  = 16,
};

enum B_FILE_REGISTERS
{
    B_SADDR = 0,
    B_SPTCH,
    B_DADDR,
    B_DPTCH,
    B_OFFSET,
    B_WSTART,
    B_WEND,
    B_DYDX,
    B_COLOR0,
    B_COLOR1,
    B_COUNT,
    B_INC1,
    B_INC2,
    B_PATTRN,
    B_TEMP,
};

enum STATUS_FLAGS
{
    ST_NONE         = 0,
    ST_FS0_MASK     = 0x1F,
    ST_FS0_SHIFT    = 0,
    ST_FE0          = 0x20,
    ST_FS1_MASK     = 0x7C0,
    ST_FS1_SHIFT    = 6,
    ST_FE1          = 0x800,
    ST_IE           = 0x200000,
    ST_PBX          = 0x1000000,
    ST_V            = 0x10000000,
    ST_Z            = 0x20000000,
    ST_C            = 0x40000000,
    ST_N            = 0x80000000,
};

enum IO_REGISTERS
{
    IO_REFCNT   = 0x0C00001F0,
    IO_DPYADR   = 0x0C00001E0,
    IO_VCOUNT   = 0x0C00001D0,
    IO_HCOUNT   = 0x0C00001C0,
    IO_DPYTAP   = 0x0C00001B0,
    IO_PMASK    = 0x0C0000160,
    IO_PSIZE    = 0x0C0000150,
    IO_CONVDP   = 0x0C0000140,
    IO_CONVSP   = 0x0C0000130,
    IO_INTPEND  = 0x0C0000120,
    IO_INTENB   = 0x0C0000110,
    IO_HSTCTLH  = 0x0C0000100,
    IO_HSTCTLL  = 0x0C00000F0,
    IO_HSTADRH  = 0x0C00000E0,
    IO_HSTADRL  = 0x0C00000D0,
    IO_HSTDATA  = 0x0C00000C0,
    IO_CONTROL  = 0x0C00000B0,
    IO_DPYINT   = 0X0C00000A0,
    IO_DPYSTRT  = 0X0C0000090,
    IO_DPYCTL   = 0X0C0000080,
    IO_VTOTAL   = 0X0C0000070,
    IO_VSBLNK   = 0X0C0000060,
    IO_VEBLNK   = 0X0C0000050,
    IO_VESYNC   = 0X0C0000040,
    IO_HTOTAL   = 0X0C0000030,
    IO_HSBLNK   = 0X0C0000020,
    IO_HEBLNK   = 0X0C0000010,
    IO_HESYNC   = 0X0C0000000,
};

enum TRAP_VECTORS
{
    VECT_RESET = 0xFFFFFFE0,
};

typedef enum
{
    STOP_CYCLES_EXPIRED = 0,
    STOP_BREAKPOINT_FOUND,
} stopreason_t;

const int KLUT[32] = {
    32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
};

class tms34010
{
public:
    typedef union cpureg {
        struct {
            short x;
            short y;
        };
        uint32_t value;
        cpureg() : value(0) {
        }

        cpureg(const cpureg& r) {
            value = r.value;
        }
        operator unsigned&() {
            return value;
        }
    } cpureg_t;

    struct tms34010_state {
        uint32_t pc;
        uint32_t st;
        cpureg_t sp;
        cpureg_t r[32];
    };

    tms34010_state m_state;
    uint32_t m_last_pc;

    tms34010();
    void reset();
    int run(int cycles, bool skip_bps=false);
    string dasm(uint16_t opcode, uint32_t pc, int *szbits=nullptr);
    int m_cycles;

    void bp_insert(addr_t address);
    void bp_remove(addr_t address);

    static const char *reg_names[32];
private:
    unordered_set<addr_t> m_breakpoints;
    fstream m_log;

    bool check_breakpoint();

    void ZN_UPDATE(uint32_t value);
    void Z_UPDATE(uint32_t value);

    void MOVI_w(uint16_t opcode);
    void MOVI_l(uint16_t opcode);
    void SETF(uint16_t opcode);
    void DINT(uint16_t opcode);

    void DSJS(uint16_t opcode);

    uint64_t read_field_0(bitaddr_t addr);
    uint64_t read_field_1(bitaddr_t addr);
    void write_field_0(bitaddr_t addr, uint64_t data);
    void write_field_1(bitaddr_t addr, uint64_t data);

    void MOVE_r_2_abs_0(uint16_t opcode);
    void MOVE_r_2_abs_1(uint16_t opcode);

    void MOVE_abs_2_r_0(uint16_t opcode);
    void MOVE_abs_2_r_1(uint16_t opcode);


    //------
    void ABS_rd(uint16_t opcode);
    void ADD_rs_rd(uint16_t opcode);
    void ADDC_rs_rd(uint16_t opcode);
    void ADDI_iw_rd(uint16_t opcode);
    void ADDI_il_rd(uint16_t opcode);
    void ADDK_k_rd(uint16_t opcode);
    void ADDXY_rs_rd(uint16_t opcode);
    void AND_rs_rd(uint16_t opcode);
    void ANDI_il_rd(uint16_t opcode);
    void ANDN_rs_rd(uint16_t opcode);
    void ANDNI_il_rd(uint16_t opcode);
    void BTST_k_rd(uint16_t opcode);
    void BTST_rs_rd(uint16_t opcode);
    void CLR_rd(uint16_t opcode);
    void CLRC(uint16_t opcode);
    void CMP_rs_rd(uint16_t opcode);
    void CMPI_iw_rd(uint16_t opcode);
    void CMPI_il_rd(uint16_t opcode);
    void CMPXY_rs_rd(uint16_t opcode);
    void DEC_rd(uint16_t opcode);
    void DIVS_rs_rd(uint16_t opcode);
    void DIVU_rs_rd(uint16_t opcode);
    void MODS_rs_rd(uint16_t opcode);
    void MODU_rs_rd(uint16_t opcode);
    void MPYS_rs_rd(uint16_t opcode);
    void MPYU_rs_rd(uint16_t opcode);
    void NEG_rd(uint16_t opcode);
    void NEGB_rd(uint16_t opcode);
    void NOT_rd(uint16_t opcode);
    void OR_rs_rd(uint16_t opcode);
    void ORI_il_rd(uint16_t opcode);
    void SETC(uint16_t opcode);
    void SEXT_rd_F(uint16_t opcode);
    void SUB_rs_rd(uint16_t opcode);
    void SUBB_rs_rd(uint16_t opcode);
    void SUBI_iw_rd(uint16_t opcode);
    void SUBI_il_rd(uint16_t opcode);
    void SUBK_k_rd(uint16_t opcode);
    void SUBXY_rs_rd(uint16_t opcode);
    void XOR_rs_rd(uint16_t opcode);
    void XORI_il_rd(uint16_t opcode);
    void ZEXT_rd_F(uint16_t opcode);

    void CALLA(uint16_t opcode);
    void CALLR(uint16_t opcode);

    void JACC(uint16_t opcode);
    void JRCC(uint16_t opcode);
    void JRCC_rel(uint16_t opcode);

    void RETS_n(uint16_t opcode);
    void POPST(uint16_t opcode);
    void PUSHST(uint16_t opcode);
    void MMTM_rp_list(uint16_t opcode);
    void MMFM_rp_list(uint16_t opcode);
    void MOVK_k_rd(uint16_t opcode);

    void MOVE_rs_rd(uint16_t opcode);
    void MOVE_rs_rd_ab(uint16_t opcode);
    void MOVE_rs_rd_ba(uint16_t opcode);

    // MOVE rs, *rd+, f
    void MOVE_rs_i_rd_pi_0(uint16_t opcode);
    void MOVE_rs_i_rd_pi_1(uint16_t opcode);

    // MOVE *rs+, rd, f
    void MOVE_i_rs_pi_rd_0(uint16_t opcode);
    void MOVE_i_rs_pi_rd_1(uint16_t opcode);

    // MOVE *rs+, *rd+, f
    void MOVE_i_rs_pi_i_rd_pi_0(uint16_t opcode);
    void MOVE_i_rs_pi_i_rd_pi_1(uint16_t opcode);

    // MOVE *rs, rd, f
    void MOVE_i_rs_rd_0(uint16_t opcode);
    void MOVE_i_rs_rd_1(uint16_t opcode);

    // MOVE rs, *rd, f
    void MOVE_rs_i_rd_0(uint16_t opcode);
    void MOVE_rs_i_rd_1(uint16_t opcode);

    void MOVB_i_rs_rd(uint16_t opcode);
    void MOVB_rs_i_rd(uint16_t opcode);
    void MOVB_rs_da(uint16_t opcode);
    void SLL_k_rd(uint16_t opcode);
    void SRL_k_rd(uint16_t opcode);

    void LMO_rs_rd(uint16_t opcode);

    void JUMP_rs(uint16_t opcode);
    void EXGPC_rd(uint16_t opcode);

    void MOVX_rs_rd(uint16_t opcode);
    void MOVY_rs_rd(uint16_t opcode);

    void DSJ_rd_disp(uint16_t opcode);

    template<uint8_t type> bool COND_EVAL();
    stopreason_t m_reason;
public:
    stopreason_t reason() const {
        return m_reason;
    }
};

extern tms34010 *g_tms;

}

#endif // TMS34010_H

