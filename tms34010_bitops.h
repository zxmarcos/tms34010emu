#ifndef TMS34010_BITOPS
#define TMS34010_BITOPS

#include "tms34010.h"
#include "tms34010_memacc.h"
#include "tms34010_defs.h"
#include "tms34010_helper.h"

namespace tms
{

inline void tms34010::AND_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    rd &= rs;
    Z_UPDATE(rd);
}

inline void tms34010::ANDI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = ~read32_aligned(PC);
    rd &= il;
    Z_UPDATE(rd);
    PC += 32;
}

inline void tms34010::ANDN_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    rd &= ~rs;
    Z_UPDATE(rd);
}

inline void tms34010::ANDNI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = read32_aligned(PC);
    rd &= ~il;
    Z_UPDATE(rd);
    PC += 32;
}


inline void tms34010::BTST_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    Z_UPDATE(rd & (1 << KN));
}

inline void tms34010::BTST_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    Z_UPDATE(rd & (1 << (rs & 0x1F)));
}

inline void tms34010::CLR_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    rd = 0;
    ST |= ST_Z;
}


inline void tms34010::NOT_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    rd = ~rd;
    Z_UPDATE(rd);
}

inline void tms34010::OR_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    rd |= rs;
    Z_UPDATE(rd);
}

inline void tms34010::ORI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = read32_aligned(PC);
    rd ^= il;
    Z_UPDATE(rd);
    PC += 32;
}


inline void tms34010::XOR_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    rd ^= rs;
    Z_UPDATE(rd);
}

inline void tms34010::XORI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = read32_aligned(PC);
    rd ^= il;
    Z_UPDATE(rd);
    PC += 32;
}

inline void tms34010::LMO_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();

    ST &= ~ST_Z;
    if (rs) {
        for (int i = 31; i >= 0; i--) {
            if (rs & 0x80000000) {
                rd = 31 - i;
                return;
            }
            rs <<= 1;
        }
    } else {
        ST |= ST_Z;
    }
}

}

#endif // TMS34010_BITOPS

