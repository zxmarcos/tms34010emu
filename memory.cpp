/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#include <iostream>
#include <cstdio>
#include <stdexcept>
#include "memory.h"
#include "tms34010.h"
#include "serialpic.h"


namespace tms
{

midway_serial_pic *g_sec_pic = nullptr;

void init_devices()
{
    if (g_sec_pic == nullptr)
        g_sec_pic = new midway_serial_pic(528);
    else
        g_sec_pic->reset_w();
}


namespace mem
{

static vector<mem_area> m_areas;


void register_area(const mem_area& area)
{
    printf("%08X-%08X tentando registrar em %p\n", area.address, area.address + area.size - 1, area.base_ptr);
    for (const mem_area& va : m_areas) {
        if (area.has(va.address) || area.has(va.address + va.size - 1)) {
            cout << "Não foi possível registrar a área" << endl;
            return;
        }
    }

    printf("%08X-%08X registrado em %p\n", area.address, area.address + area.size - 1, area.base_ptr);
    m_areas.push_back(area);
}

void unregister_area(const mem_area& area)
{
    for (auto it = m_areas.begin(); it != m_areas.end(); it++) {
        auto va = *it;
        if (area.has(va.address) || area.has(va.address + va.size)) {
            m_areas.erase(it);
            return;
        }
    }
}

#define IN_RANGE(x, s, e) (x >= s && x <= e)
#define BA(x)   (x>>3)

template<typename T>
inline T do_read(addr_t address) {
    address &= 0xFFFFFFFF;

    if (IN_RANGE(address, BA(0x01b00000), BA(0x01b0001f))) {
        return ~0;
    }
    else
    if (IN_RANGE(address, BA(0x01800000), BA(0x0187ffff))) {
        static short counter = 0;
        if (address == BA(0x0187ffc0))
            return 0xC00;
        else
        if (address == BA(0x0187FFB0))
            return counter++;
        else
        if (address == BA(0x0187FFA0))
            return 0xFFFFFD7D;
    }
    else
    if (IN_RANGE(address, BA(0x01600000), BA(0x0160001f))) {
        return g_sec_pic->read();
    }

    for (const mem_area& area : m_areas) {
        if (area.has(address))
            return area.fast_read<T>(address);
    }
    printf("[%08x] Invalid read at %08X [~%08X]\n", g_tms->m_last_pc, address, address * 8);
    return 0;
}

template<typename T>
inline void do_write(addr_t address, T value) {
    address &= 0xFFFFFFFF;

    if (IN_RANGE(address, BA(0x01600000), BA(0x0160001f))) {
        g_sec_pic->write(value);
        return;
    }

    if (IN_RANGE(address, BA(0x00000), BA(0x3fffff))) {
        printf("local_vram[%08x - %08x] = %x\n", address, address*8, value);
    }

    for (mem_area& area : m_areas) {
        if (area.has(address))  {
            area.fast_write<T>(address, value);
            return;
        }
    }
    printf("[%08x] Invalid write at %08X [%08X] <- %08X\n", g_tms->m_last_pc, address, address * 8, value);
}

uint8_t read_byte(addr_t address)
{
    return do_read<uint8_t>(address);
}

uint16_t read_word(addr_t address)
{
    return do_read<uint16_t>(address);
}

uint32_t read_dword(addr_t address)
{
    return do_read<uint32_t>(address);
}

uint64_t read_qword(addr_t address)
{
    return do_read<uint64_t>(address);
}

void write_byte(addr_t address, uint8_t value)
{
    do_write(address, value);
}

void write_word(addr_t address, uint16_t value)
{
    do_write(address, value);
}

void write_dword(addr_t address, uint32_t value)
{
    do_write(address, value);
}

void write_qword(addr_t address, uint64_t value)
{
    do_write(address, value);
}

const vector<mem_area> *get_areas()
{
    return &m_areas;
}



}
}

