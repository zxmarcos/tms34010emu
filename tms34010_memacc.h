/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_MEMACC
#define TMS34010_MEMACC

#include <cstdint>
#include "tms34010.h"
#include "tms34010_defs.h"
#include "memory.h"

#define __memaddr(x)    ((x) >> 3)
#define __be16(x)       __builtin_bswap16(x)

namespace tms
{

static inline uint8_t read8_aligned(bitaddr_t addr)
{
    return mem::read_byte(__memaddr(addr & ~0xF));
}

static inline int32_t read8_aligned_sx(bitaddr_t addr)
{
    return (int32_t)(int8_t) read8_aligned(addr);
}

static inline uint16_t read16_aligned(bitaddr_t addr)
{
    return mem::read_word(__memaddr(addr & ~0xF));
}

static inline int32_t read16_aligned_sx(bitaddr_t addr)
{
    return (int32_t)(int16_t) read16_aligned(addr);
}

static inline uint32_t read32_aligned(bitaddr_t addr)
{
    return mem::read_dword(__memaddr(addr & ~0xF));
}

static inline uint64_t read64_aligned(bitaddr_t addr)
{
    return mem::read_qword(__memaddr(addr & ~0xF));
}

static inline void write8_aligned(bitaddr_t addr, uint8_t data)
{
    mem::write_byte(__memaddr(addr & ~0xF), data);
}

static inline void write64_aligned(bitaddr_t addr, uint64_t data)
{
    mem::write_qword(__memaddr(addr & ~0xF), data);
}

static inline void write32_aligned(bitaddr_t addr, uint64_t data)
{
    mem::write_dword(__memaddr(addr & ~0xF), data);
}


/* FEDCBA98 76543210 | FEDCBA98 76543210 | FEDCBA98 76543210 | FEDCBA98 76543210
 * 00000000 00000000 | 00000000 00000000 | 00000000 00000000 | 00000000 00000000
 *         W4        |         W3        |         W2        |         W1
 *
 * */

// Field read
inline uint64_t tms34010::read_field_0(bitaddr_t addr)
{
    uint64_t mask = U64(0xFFFFFFFFFFFFFFFF) << FS0;
    unsigned bitadr = addr & 0xF;
    uint64_t data = (read64_aligned(addr) >> bitadr);

//    printf("RFLD_0[%08h] %X, [mask = %x] %X\n", addr, data, mask, FS0);

    data &= ~mask;
    if (FS0_SEXT) {
        if (data & (1 << (FS0 - 1)))
            data |= mask;
    }
    return data;
}

inline uint64_t tms34010::read_field_1(bitaddr_t addr)
{
    uint64_t mask = U64(0xFFFFFFFFFFFFFFFF) << FS1;
    unsigned bitadr = addr & 0xF;
    uint64_t data = (read64_aligned(addr) >> bitadr);

//    printf("RFLD_1 %X, [mask = %x] %X\n", data, mask, FS1);

    data &= ~mask;
    if (FS1_SEXT) {
        if (data & (1 << (FS1 - 1)))
            data |= mask;
    }
    return data;
}

inline void tms34010::write_field_0(bitaddr_t addr, uint64_t data)
{
    unsigned fshift = ST & ST_FS0_MASK;
    unsigned bitadr = addr & 0xF;
    uint64_t mask = (U64(0xFFFFFFFF) >> fshift) << bitadr;
    uint64_t old = read64_aligned(addr);
    data = (old & ~mask) | ((data << bitadr) & mask);
    write64_aligned(addr, data);
}

inline void tms34010::write_field_1(bitaddr_t addr, uint64_t data)
{
    unsigned fshift = (ST & ST_FS1_MASK) >> ST_FS1_SHIFT;
    unsigned bitadr = addr & 0xF;
    uint64_t mask = (U64(0xFFFFFFFF) >> fshift) << bitadr;
    uint64_t old = read64_aligned(addr);
    data = (old & ~mask) | ((data << bitadr) & mask);
    write64_aligned(addr, data);
}


}

#endif // TMS34010_MEMACC

