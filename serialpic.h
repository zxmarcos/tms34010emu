#ifndef SERIALPIC
#define SERIALPIC

#include <cstdint>

class midway_serial_pic
{
    void generate_serial_data(int upper);

    uint8_t   m_data[16]; // reused by other devices
    int     m_upper;
    uint8_t   m_buff;
    uint8_t   m_idx;
    uint8_t   m_status;
    uint8_t   m_bits;
    uint8_t   m_ormask;
public:
    midway_serial_pic(int upper);
    void reset_w();
    uint8_t status_r();
    uint8_t read();
    void write(uint8_t data);
};

#endif // SERIALPIC

