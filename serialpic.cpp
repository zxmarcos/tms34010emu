/* Ported from MAME */
#include <cstdint>
#include <cstdlib>
#include "serialpic.h"

void midway_serial_pic::generate_serial_data(int upper)
{
    int year = 1994, month = 12, day = 11;
    uint32_t serial_number, temp;
    uint8_t serial_digit[9];

    serial_number = 123456;
    serial_number += upper * 1000000;

    serial_digit[0] = (serial_number / 100000000) % 10;
    serial_digit[1] = (serial_number / 10000000) % 10;
    serial_digit[2] = (serial_number / 1000000) % 10;
    serial_digit[3] = (serial_number / 100000) % 10;
    serial_digit[4] = (serial_number / 10000) % 10;
    serial_digit[5] = (serial_number / 1000) % 10;
    serial_digit[6] = (serial_number / 100) % 10;
    serial_digit[7] = (serial_number / 10) % 10;
    serial_digit[8] = (serial_number / 1) % 10;

    m_data[12] = rand() & 0xff;
    m_data[13] = rand() & 0xff;

    m_data[14] = 0; /* ??? */
    m_data[15] = 0; /* ??? */

    temp = 0x174 * (year - 1980) + 0x1f * (month - 1) + day;
    m_data[10] = (temp >> 8) & 0xff;
    m_data[11] = temp & 0xff;

    temp = serial_digit[4] + serial_digit[7] * 10 + serial_digit[1] * 100;
    temp = (temp + 5 * m_data[13]) * 0x1bcd + 0x1f3f0;
    m_data[7] = temp & 0xff;
    m_data[8] = (temp >> 8) & 0xff;
    m_data[9] = (temp >> 16) & 0xff;

    temp = serial_digit[6] + serial_digit[8] * 10 + serial_digit[0] * 100 + serial_digit[2] * 10000;
    temp = (temp + 2 * m_data[13] + m_data[12]) * 0x107f + 0x71e259;
    m_data[3] = temp & 0xff;
    m_data[4] = (temp >> 8) & 0xff;
    m_data[5] = (temp >> 16) & 0xff;
    m_data[6] = (temp >> 24) & 0xff;

    temp = serial_digit[5] * 10 + serial_digit[3] * 100;
    temp = (temp + m_data[12]) * 0x245 + 0x3d74;
    m_data[0] = temp & 0xff;
    m_data[1] = (temp >> 8) & 0xff;
    m_data[2] = (temp >> 16) & 0xff;

    /* special hack for RevX */
    m_ormask = 0x80;
    if (upper == 419)
        m_ormask = 0x00;
}

midway_serial_pic::midway_serial_pic(int upper) : m_upper(upper)
{
    reset_w();
    generate_serial_data(m_upper);

}


void midway_serial_pic::reset_w()
{
    m_buff = 0;
    m_idx = 0;
    m_status = 0;
    m_bits = 0;
    m_ormask = 0;
}


uint8_t midway_serial_pic::status_r()
{
    return m_status;
}

uint8_t midway_serial_pic::read()
{
    m_status = 1;
    return m_buff;
}


void midway_serial_pic::write(uint8_t data)
{
    /* status seems to reflect the clock bit */
    m_status = (data >> 4) & 1;

    /* on the falling edge, clock the next data byte through */
    if (!m_status)
    {
        /* the self-test writes 1F, 0F, and expects to read an F in the low 4 bits */
        /* Cruis'n World expects the high bit to be set as well */
        if (data & 0x0f)
            m_buff = m_ormask | data;
        else
            m_buff = m_data[m_idx++ % sizeof(m_data)];
    }
}

