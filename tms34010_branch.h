#ifndef TMS34010_BRANCH
#define TMS34010_BRANCH

#include "tms34010.h"
#include "tms34010_memacc.h"
#include "tms34010_defs.h"
#include "tms34010_helper.h"

namespace tms
{

inline void tms34010::DSJS(uint16_t opcode)
{
    if (--RD_r)
        PC += (BKW_DIR ? -OFFS : OFFS) * 16;
}


inline void tms34010::DSJ_rd_disp(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    if (--rd) {
        PC += 16 + read16_aligned_sx(PC) * 16;
    } else {
        PC += 16;
    }
}

inline void tms34010::CALLA(uint16_t opcode)
{
    uint32_t adr = read32_aligned(PC);
    SP_v -= 32;
    write32_aligned(SP_v, PC + 32);
    PC = adr;
}

inline void tms34010::CALLR(uint16_t opcode)
{
    uint32_t next = PC + 16;
    uint32_t adr = next + read16_aligned_sx(PC) * 16;
    SP_v -= 32;
    write32_aligned(SP_v, next);
    PC = adr;
}

#define CHECK_CONDITION(taken)                      \
    do {                                            \
    switch((opcode >> 8) & 0xF) {                   \
    case 0x00: taken = COND_EVAL<0x00>(); break;    \
    case 0x01: taken = COND_EVAL<0x01>(); break;    \
    case 0x02: taken = COND_EVAL<0x02>(); break;    \
    case 0x03: taken = COND_EVAL<0x03>(); break;    \
    case 0x04: taken = COND_EVAL<0x04>(); break;    \
    case 0x05: taken = COND_EVAL<0x05>(); break;    \
    case 0x06: taken = COND_EVAL<0x06>(); break;    \
    case 0x07: taken = COND_EVAL<0x07>(); break;    \
    case 0x08: taken = COND_EVAL<0x08>(); break;    \
    case 0x09: taken = COND_EVAL<0x09>(); break;    \
    case 0x0A: taken = COND_EVAL<0x0A>(); break;    \
    case 0x0B: taken = COND_EVAL<0x0B>(); break;    \
    case 0x0C: taken = COND_EVAL<0x0C>(); break;    \
    case 0x0D: taken = COND_EVAL<0x0D>(); break;    \
    case 0x0E: taken = COND_EVAL<0x0E>(); break;    \
    case 0x0F: taken = COND_EVAL<0x0F>(); break;    \
    }                                               \
    } while (0)

inline void tms34010::JACC(uint16_t opcode)
{
    bool taken = false;
    CHECK_CONDITION(taken);
    if (taken)
        PC = read32_aligned(PC) & ~0xF;
    else
        PC += 32;
}

inline void tms34010::JRCC(uint16_t opcode)
{
    bool taken = false;
    CHECK_CONDITION(taken);
    // HACK HACK HACK
    // MK3 wait here for MIDWAY PIC
    if (LPC == 0xFFB093A0 || LPC == 0xFFB0A790)
        taken = true;
    if (LPC == 0xFFB09960)
        taken = false;

    if (taken) {
#if 0
        int displace = (opcode & 0xFF) * 16;
        if (BKW_DIR)
            displace = -displace;
        PC += displace;
#else
        PC += ((int8_t)(opcode & 0xFF)) * 16;
#endif
    }
}


inline void tms34010::JRCC_rel(uint16_t opcode)
{
    bool taken = false;
    CHECK_CONDITION(taken);

    if (taken) {
        PC += 16 + read16_aligned_sx(PC) * 16;
    } else {
        PC += 16;
    }
}

inline void tms34010::JUMP_rs(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    PC = rd;
}


}

#endif // TMS34010_BRANCH

