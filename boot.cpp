/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#include <fstream>
#include <stdexcept>
#include <iostream>
#include <cstdio>
#include <cstring>
#include "memory.h"
#include "tms34010.h"

#define TOBYTE(x)   ((x) >> 3)

namespace tms
{

static uint8_t *boot_rom;
static uint8_t *main_ram;
static uint8_t *nvram;
static uint8_t *vram;
static const size_t boot_size = 0x80000;
static const size_t ram_size = TOBYTE(0x3fffff+1);
static const size_t nvram_size = TOBYTE(0x5ffff+1);
static const size_t vram_size = TOBYTE(0x3fffff+1);

void load_boot_rom()
{
    try {
        boot_rom = new uint8_t[boot_size * 2];
        main_ram = new uint8_t[ram_size];
        nvram = new uint8_t[nvram_size];
        vram = new uint8_t[vram_size];
        uint8_t *buffer = new uint8_t[boot_size];

        memset(nvram, 0, nvram_size);

        ifstream file;
        file.open("mk321u54.bin");
        file.read(reinterpret_cast<char*>(buffer), boot_size);
        file.close();

        for (size_t i = 0; i < boot_size; i++)
            boot_rom[i*2+1] = buffer[i];

        file.open("mk321u63.bin");
        file.read(reinterpret_cast<char*>(buffer), boot_size);
        file.close();

        for (size_t i = 0; i < boot_size; i++)
            boot_rom[i*2] = buffer[i];

        uint16_t *pb = (uint16_t*)boot_rom;
        for (int i = 0; i < boot_size; i++)
            pb[i] = __builtin_bswap16(pb[i]);

        delete [] buffer;

#if 0
        ofstream out("dump.bin", ios_base::trunc);
        out.write(reinterpret_cast<char*>(boot_rom), boot_size * 2);
        out.close();
#endif

        cout << "ROM loaded" << endl;


        mem::mem_area area;
        area.address = 0x1FF00000;
        area.size = boot_size * 2;
        area.base_ptr = boot_rom;
        area.flags = mem::MEM_AREA_ROM;
        mem::register_area(area);



        area.address = TOBYTE(0x01000000);
        area.size = ram_size;
        area.base_ptr = main_ram;
        area.flags = mem::MEM_AREA_RAM;
        mem::register_area(area);

        area.address = TOBYTE(0x01400000);
        area.size = nvram_size;
        area.base_ptr = nvram;
        area.flags = mem::MEM_AREA_RAM;
        mem::register_area(area);

        area.address = TOBYTE(0);
        area.size = vram_size;
        area.base_ptr = vram;
        area.flags = mem::MEM_AREA_RAM;
        mem::register_area(area);


    } catch (exception& e) {
        cerr << "Failed to load ROM: " << e.what() << endl;
        exit(-1);
    }
}

void save_vram()
{
    ofstream file("vram.bin", ios_base::out | ios_base::trunc);
    file.write((char*)vram, vram_size);
}

}
