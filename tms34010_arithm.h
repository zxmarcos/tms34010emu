/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_ARITHM
#define TMS34010_ARITHM

#include "tms34010.h"
#include "tms34010_defs.h"

// check for overflow and carry
#define ADD_VC_CHECK(sum, a, b)                 \
    do {                                        \
        ST &= ~(ST_C | ST_V);                   \
        if ((sum ^ a) & (sum ^ b) & SIGN_BIT32) \
            ST |= ST_V;                         \
        if (sum >= U64(0x100000000))            \
            ST |= ST_C;                         \
    } while (0)



namespace tms
{

inline void tms34010::ABS_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t x = (~rd + 1) & SIGN_BIT32;
    if (x & SIGN_BIT32)
        rd = x;
    if (rd == 0x80000000)
        ST |= ST_V;
    else
        ST &= ~ST_V;
    ZN_UPDATE(rd);
}

inline void tms34010::ADD_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    uint64_t sum = rd + rs;

    ADD_VC_CHECK(sum, rd, rs);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::ADDC_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v();
    uint64_t sum = rd + rs + (ST & ST_C) ? 1 : 0;

    ADD_VC_CHECK(sum, rd, rs);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::ADDI_iw_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t iw = read16_aligned_sx(PC);
    uint64_t sum = rd + iw;

    ADD_VC_CHECK(sum, rd, iw);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 16;
}

inline void tms34010::ADDI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = read32_aligned(PC);
    uint64_t sum = rd + il;

    ADD_VC_CHECK(sum, rd, il);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 32;
}

inline void tms34010::ADDK_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t k = K;
    uint64_t sum = rd + k;

    ADD_VC_CHECK(sum, rd, k);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::ADDXY_rs_rd(uint16_t opcode)
{
    cpureg_t &rd = GET_RD_r();
    cpureg_t rs = GET_RS_r();

    rd.x += rs.x;
    rd.y += rs.y;

    ST &= ~(ST_V | ST_C | ST_Z | ST_N);
    if (!rd.x)
        ST |= ST_N;
    if (rd.y & SIGN_BIT16)
        ST |= ST_C;
    if (!rd.y)
        ST |= ST_Z;
    if (rd.x & SIGN_BIT16)
        ST |= ST_V;
}


inline void tms34010::CMP_rs_rd(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t rs = (GET_RS_v()) ^ 0xFFFFFFFF;
    uint64_t sum = rd + rs + 1;

    ADD_VC_CHECK(sum, rd, rs);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::CMPI_iw_rd(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t iw = ~read16_aligned_sx(PC) ^ 0xFFFFFFFF;
    uint64_t sum = rd + iw + 1;

    ADD_VC_CHECK(sum, rd, iw);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 16;
}

inline void tms34010::CMPI_il_rd(uint16_t opcode)
{
    uint32_t rd = GET_RD_v();
    uint32_t il = ~read32_aligned(PC) ^ 0xFFFFFFFF;
    uint64_t sum = rd + il + 1;

    ADD_VC_CHECK(sum, rd, il);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 32;
}

inline void tms34010::CMPXY_rs_rd(uint16_t opcode)
{

}

inline void tms34010::DEC_rd(uint16_t opcode)
{

}

inline void tms34010::DIVS_rs_rd(uint16_t opcode)
{

}

inline void tms34010::DIVU_rs_rd(uint16_t opcode)
{
    ST &= ~(ST_Z | ST_V);

    if (RD_n & 1) {
        uint32_t &rd = GET_RD_v();
        uint32_t rs = GET_RS_v();

        if (!rs) {
            ST |= ST_V;
            rd = 0;
        }
        rd = rd / rs;
        if (!rd)
            ST |= ST_V;

    } else {
        uint32_t &rd0 = RD_v;
        uint32_t &rd1 = m_state.r[RD_n + 1];
        uint32_t rs = GET_RS_v();

        uint32_t div = 0;
        uint32_t mod = 0;

        if (rs) {

            uint64_t num = rd1;
            num |= (rd0) << 32;
            const uint64_t rs64 = static_cast<uint64_t>(rs);

            div = (uint32_t)(num / rs64);
            mod = (uint32_t)(num % rs64);

            if (!div)
                ST |= ST_Z;

        } else {
            ST |= ST_V;
        }
        rd0 = div;
        rd1 = mod;

    }
}


inline void tms34010::MODS_rs_rd(uint16_t opcode)
{

}

inline void tms34010::MODU_rs_rd(uint16_t opcode)
{

}

inline void tms34010::MPYS_rs_rd(uint16_t opcode)
{

}

inline void tms34010::MPYU_rs_rd(uint16_t opcode)
{
    ST &= ~ST_Z;

    if (RD_n & 1) {
        uint32_t &rd = GET_RD_v();
        uint32_t rs = GET_RS_v();

        rs &= FS1_MASK;

        uint64_t mul = (uint64_t)rs * (uint64_t)rd;
        if (!mul)
            ST |= ST_Z;
        rd = mul;

    } else {
        uint32_t &rd0 = RD_v;
        uint32_t &rd1 = m_state.r[RD_n + 1];
        uint32_t rs = GET_RS_v();

        rs &= FS1_MASK;

        uint64_t mul = (uint64_t)rs * (uint64_t)rd0;
        if (!mul)
            ST |= ST_Z;
        rd1 = mul;
        rd0 = mul >> 32;
    }

}

inline void tms34010::NEG_rd(uint16_t opcode)
{

}

inline void tms34010::NEGB_rd(uint16_t opcode)
{

}

inline void tms34010::SEXT_rd_F(uint16_t opcode)
{

}

inline void tms34010::SUB_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = GET_RS_v() ^ 0xFFFFFFFF;
    uint64_t sum = rd + rs + 1;

    ADD_VC_CHECK(sum, rd, rs);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::SUBB_rs_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t rs = (GET_RS_v()) ^ 0xFFFFFFFF;
    uint64_t sum = rd + rs + 1 + (ST & ST_C) ? 1 : 0;

    ADD_VC_CHECK(sum, rd, rs);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::SUBI_iw_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t iw = ~read16_aligned_sx(PC) ^ 0xFFFFFFFF;
    uint64_t sum = rd + iw + 1;

    ADD_VC_CHECK(sum, rd, iw);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 16;
}

inline void tms34010::SUBI_il_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t il = ~read32_aligned(PC) ^ 0xFFFFFFFF;
    uint64_t sum = rd + il + 1;

    ADD_VC_CHECK(sum, rd, il);
    rd = sum;
    ZN_UPDATE(rd);
    PC += 32;
}

inline void tms34010::SUBK_k_rd(uint16_t opcode)
{
    uint32_t &rd = GET_RD_v();
    uint32_t k = ~K;
    uint64_t sum = rd + k + 1;

    ADD_VC_CHECK(sum, rd, k);
    rd = sum;
    ZN_UPDATE(rd);
}

inline void tms34010::SUBXY_rs_rd(uint16_t opcode)
{
    cpureg_t &rd = GET_RD_r();;
    cpureg_t rs = GET_RS_r();;

    rd.x -= rs.x;
    rd.y -= rs.y;

    ST &= ~(ST_V | ST_C | ST_Z | ST_N);
    if (!rd.x)
        ST |= ST_N;
    if (rd.y & SIGN_BIT16)
        ST |= ST_C;
    if (!rd.y)
        ST |= ST_Z;
    if (rd.x & SIGN_BIT16)
        ST |= ST_V;
}


inline void tms34010::ZEXT_rd_F(uint16_t opcode)
{

}


}

#endif // TMS34010_ARITHM

