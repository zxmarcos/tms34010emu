/*
 * Copyright (c) 2015, Marcos Medeiros
 * Licensed under BSD 3-clause.
 */
#ifndef TMS34010_DEFS
#define TMS34010_DEFS

#define U64(x) x##ULL

// xxxx xxxS SSSR DDDD
// RS = RSSSS
// RD = RDDDD
#define R_BIT   (opcode & 0x10)
#define RS      (((opcode >> 5) & 0xF) | R_BIT)
#define RD      ((opcode & 0xF) | R_BIT)
#define RS_n    ((opcode >> 5) & 0xF)
#define RD_n    (opcode & 0xF)

#define NF      (opcode & 0x1F)

#define FS0         KLUT[((ST & ST_FS0_MASK) >> ST_FS0_SHIFT)]
#define FS0_MASK    (0xFFFFFFFF >> (32 - FS0))
#define FS0_SEXT    (ST & ST_FE0)
#define FS1         KLUT[((ST & ST_FS1_MASK) >> ST_FS1_SHIFT)]
#define FS1_SEXT    (ST & ST_FE1)
#define FS1_MASK    (0xFFFFFFFF >> (32 - FS1))

#define OFFS    ((opcode >> 5) & 0x1F)
#define K       KLUT[((opcode >> 5) & 0x1F)]
#define KN      ((~opcode >> 5) & 0x1F)
#define K2C     ((-K) & 0x1F)
#define M_BIT   (opcode & (1 << 9))

#define RD_v    m_state.r[RD].value
#define RS_v    m_state.r[RS].value
#define RD_r    m_state.r[RD]
#define RS_r    m_state.r[RS]

#define GET_RD_v()  (RD_n == R_SP) ? SP_v : RD_v
#define GET_RS_v()  (RS_n == R_SP) ? SP_v : RS_v

#define GET_RD_r()  (RD_n == R_SP) ? SP_r : RD_r
#define GET_RS_r()  (RS_n == R_SP) ? SP_r : RS_r

#define GET_RD_A()  (RD_n == R_SP) ? SP_r : A(RD_n)
#define GET_RD_B()  (RD_n == R_SP) ? SP_r : B(RD_n)
#define GET_RS_A()  (RS_n == R_SP) ? SP_r : A(RS_n)
#define GET_RS_B()  (RS_n == R_SP) ? SP_r : B(RS_n)

#define CC     (ST & ST_C)
#define ZZ     (ST & ST_Z)
#define NN     (ST & ST_N)
#define VV     (ST & ST_V)

// backward direction
#define BKW_DIR (opcode & (1 << 10))

#define PC      m_state.pc
#define LPC     m_last_pc
#define ST      m_state.st
#define SP_r    m_state.sp
#define SP_v    m_state.sp.value
#define A(x)    m_state.r[x].value
#define B(x)    m_state.r[R_B + (x)].value

#define CYCLES  m_cycles

#define SIGN_BIT32    0x80000000
#define SIGN_BIT16    0x00008000

#endif // TMS34010_DEFS

